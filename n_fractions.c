//WAP to find the sum of n fractions.
#include<stdio.h>
typedef struct fract
{
 int num,den;
}Fract;
int num()
{
	int val;
	printf("Enter the number of Fractions : \n");
	scanf("%d",&val);
	if( val < 0)
    {
        printf("Enter a positive number : \n");
        return num();
    }
    return val;
}
void input(Fract *a, int n )
{
	for(int i=0;i<n;i++)
	{
		printf("Enter the numerator:\n");
		scanf("%d",&a[i].num);
		printf("Enter the denominator: \n");
		scanf("%d",&a[i].den);
	}
}
void display_result(Fract temp, Fract *a, int n)
{
	printf("The sum of the fractions ");
	for(int i=0;i<n;i++)
	{
		if(i==n-1)
		{
			printf("%d/%d = %d/%d", a[i].num, a[i].den,temp.num,temp.den);
		}
		else
		{
			printf("%d/%d +",a[i].num,a[i].den);
		}
	}
}
int gcd(int x,int y)
{
	if(x==0)
	{
		return y;
	}
	return gcd(y%x,x);
}
Fract compute(int n, Fract *a)
{
	Fract res;
	int x, y=0, test=1;
	for(int i=0; i<n;i++)
	{
		test =test*a[i].den;
	}
	res.den=test;

	for(int i=0;i<n;i++)
	{
		x=a[i].num;
		for(int j=0;j<n;j++)
		{
			if(i!=j)
			{
				x=x*a[j].den;
			}
		}
		y=y+x;
	}
	res.num=y/gcd(y,test);
	res.den=test/gcd(y,test);
	return res;
}
int main()
{
    int n;
    Fract res;
    n=num();
    Fract a[n];
    input(a,n);
    res=compute(n,a);
    display_result(res,a,n);
    return 0;
}




