//Write a program to find the sum of n different numbers using 4 functions
#include<stdio.h>
int input_num()
{
    int n;
    printf("Enter the number of elements to be added:\n");
    scanf("%d",&n);
    return n;
}
void input_arr(int n, int a[n])
{
    for(int i=0;i<n;i++)
    {
        printf("Enter the element %d :\n",i+1);
        scanf("%d",&a[i]);
    }
}
int sum_arr(int n, int a[n])
{
    int sum=0;
    for(int i=0;i<n;i++) 
    {
        sum += a[i];
    }
    return sum;
}
int display_res(int n, int a[n], int sum)
{
    int i;
    printf("The sum of ");
    for(i=0;i<n-1;i++)
    {
        printf("%d + ",a[i]);
    }
    printf("%d = %d", a[i],sum);
}
int main()
{
    int n,sum;
    n = input_num();
    int a[n];
    input_arr(n,a);
    sum=sum_arr(n,a);
    display_res(n,a,sum);
}

