//WAP to find the distance between two points using structures and 4 functions.
#include <stdio.h>
#include <math.h>
struct point
{
    float x;
    float y;
};
typedef struct point Point1;
Point1 input()
{
    Point1 p;
    printf("Enter abcissa :\n ");
    scanf("%f",&(p.x));
    printf("Enter ordinate :\n ");
    scanf("%f",&p.y);
    return p;
}
float compute_point(Point1 p1,Point1 p2)
{
    float distance;
    distance=sqrt(pow((p1.x-p2.x),2)+pow((p1.y-p2.y),2));
    return distance;
}
void output(Point1 p1, Point1 p2, float dist)
{
printf("The distance between %f,%f and %f,%f is %f \n",p1.x,p1.y,p2.x,p2.y,dist);
}
int main()
{
    float dist;
    Point1 p1,p2;
    printf("Enter the co ordinate of 1st point : \n");
    p1=input();
    printf("Enter the co ordinate of 2nd point : \n");
    p2=input();
    dist=compute_point(p1,p2);
    output(p1,p2,dist);
    return 0;
}

