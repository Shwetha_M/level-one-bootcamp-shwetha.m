//WAP to find the sum of two fractions.
#include<stdio.h>
struct fract
{
    int n;
    int d;
};
typedef struct fract fraction;
fraction input()
{
    fraction a;
    printf("Enter the numerator:\n");
    scanf("%d",&a.n);
    printf("Enter the denominator:\n");
    scanf("%d",&a.d);
    return a;
}
int euclid_gcd(int a, int b)
{
 int gcd;
 int remainder;
 if(a==0)
      {
       return(b);
      }
 else if(b==0)
      {
       return(a);
      }
 else
      {
       while(b!=0)
          {
	       remainder=a%b;
	       a=b;
	       b=remainder;
          }
 return(a);
     }
}
fraction compute(fraction a, fraction b)
{
    int gcd, i, num, den;
    num=(a.n*b.d)+(a.d*b.n);
    den=a.d*b.d;
    gcd=euclid_gcd(num,den);
    fraction sum;
    sum.n=num/gcd;
    sum.d=den/gcd;
    return sum;
}
void output(fraction sum,fraction a,fraction b)
{
    printf("The sum of two fraction %d/%d and %d/%d is %d/%d",a.n,a.d,b.n,b.d,sum.n,sum.d);
}
int main()
{
    fraction a,b,sum;
    a=input();  
    b=input();
    sum=compute(a,b);
    output(sum,a,b);
    return 0;
}

